import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Layout, Icon } from 'antd';
import { Redirect } from 'react-router-dom';
import PrivateLayoutWrapper from './styles';
import { CustomHeader, CustomSlider } from '../../components/common';
import { PUBLIC_ROUTES } from '../../routes/PublicRoutes';

const { Footer, Content } = Layout;

const MOBILE_TABS = [
  {
    key: 'home',
    text: 'Profile',
    url: '/',
    icon: 'home'
  },
  {
    key: 'user',
    text: 'Profile',
    url: '#',
    icon: 'user'
  }
];

const PrivateLayout = ({ isAuthenticated, children, location }) => {
  if (!PUBLIC_ROUTES.find(e => location.pathname.search(e.path) > -1)) {
    return !isAuthenticated ? (
      <Redirect to="/login" />
    ) : (
      <PrivateLayoutWrapper>
        <Layout className="windowView">
          <CustomSlider />
          <Layout className="mainView">
            <CustomHeader />
            <Content className="container">
              <div className="content">{children}</div>
              <Footer className="footerMobile">
                {MOBILE_TABS.map(tab => (
                  <a href={tab.url} key={tab.key}>
                    <Icon type={tab.icon} className="tabIcon" />
                  </a>
                ))}
              </Footer>
            </Content>
          </Layout>
        </Layout>
      </PrivateLayoutWrapper>
    );
  }
  return null;
};

PrivateLayout.propTypes = {
  children: PropTypes.array.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired
};

export default connect(state => ({
  isAuthenticated: state.auth.isAuthenticated,
  isLoading: state.loading.isLoading,
  location: state.router.location
}))(PrivateLayout);
