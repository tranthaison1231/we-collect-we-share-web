import { takeEvery, put, take } from 'redux-saga/effects';
import firebase from 'firebase';
import { eventChannel } from 'redux-saga';
import {
  getAllManagerSourceSuccess,
  ManagerSourceTypes,
  createSourceFailAction,
  deleteSourceFailAction,
  editSourceFailAction,
  getSourcesFailAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

let channel = null;

function* getAllManagerSourceSaga() {
  try {
    if (channel !== null) channel.close();
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('sources')
        .orderBy('name')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      let data = [];
      docs.forEach(doc => {
        data = [...data, { id: doc.id, data: doc.data() }];
      });
      yield put(getAllManagerSourceSuccess(data));
      yield put(defaultButtonsAction());
    }
  } catch (error) {
    console.error(error);
  }
}

function* createSourceSaga({ source }) {
  try {
    yield firebase
      .firestore()
      .collection('sources')
      .add(source);
  } catch (e) {
    yield put(createSourceFailAction());
  }
}

function* deleteSourceSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('sources')
      .doc(id)
      .delete();
  } catch (e) {
    yield put(deleteSourceFailAction(e));
  }
}

function* editSourceSaga({ id, source }) {
  try {
    yield firebase
      .firestore()
      .collection('sources')
      .doc(id)
      .update(source);
  } catch (e) {
    yield put(editSourceFailAction(e));
  }
}

function* getNextSourcesSaga({ lastSource }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('sources')
      .orderBy('name')
      .startAfter(lastSource.data.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('sources')
          .orderBy('name')
          .startAfter(lastSource.data.name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllManagerSourceSuccess(data));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getSourcesFailAction(e));
  }
}

function* getPrevSourcesSaga({ firstSource }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('sources')
      .orderBy('name', 'desc')
      .startAfter(firstSource.data.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('sources')
          .orderBy('name')
          .startAt(response.docs[9].data().name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        let data = [];
        docs.forEach(doc => {
          data = [...data, { id: doc.id, data: doc.data() }];
        });
        yield put(getAllManagerSourceSuccess(data));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getSourcesFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}
export default [
  takeEvery(ManagerSourceTypes.GET_ALL_MANAGER_SOURCE, getAllManagerSourceSaga),
  takeEvery(ManagerSourceTypes.CREATE_SOURCE, createSourceSaga),
  takeEvery(ManagerSourceTypes.DELETE_SOURCE, deleteSourceSaga),
  takeEvery(ManagerSourceTypes.EDIT_SOURCE, editSourceSaga),
  takeEvery(ManagerSourceTypes.GET_NEXT_SOURCES, getNextSourcesSaga),
  takeEvery(ManagerSourceTypes.GET_PREV_SOURCES, getPrevSourcesSaga),
  takeEvery(ManagerSourceTypes.UNSUBSCRIBE, unSubscribeSaga),
];
