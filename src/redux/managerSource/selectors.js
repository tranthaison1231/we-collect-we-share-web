import { createSelector } from 'reselect';

const sourcesByPageSelector = state => state.managerSource.managerSources;

export const lastSourceSelector = createSelector(
  sourcesByPageSelector,
  sourcesByPage => (sourcesByPage[9] ? sourcesByPage[9] : null)
);

export const firstSourceSelector = createSelector(
  sourcesByPageSelector,
  sourcesByPage => (sourcesByPage[0] ? sourcesByPage[0] : null)
);
