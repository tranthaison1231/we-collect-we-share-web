import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const FirebaseImgTypes = makeConstantCreator(
  'UPLOAD_IMAGE',
  'GET_IMAGE_URL'
);

export const uploadImageAction = (file, onSuccess) =>
  makeActionCreator(FirebaseImgTypes.UPLOAD_IMAGE, { file, onSuccess });

export const getImageUrlAction = (imgName, setURL) =>
  makeActionCreator(FirebaseImgTypes.GET_IMAGE_URL, { imgName, setURL });
