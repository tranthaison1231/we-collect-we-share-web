import { createSelector } from 'reselect';

const employeesByPageSelector = state => state.employees.employeesByPage;

export const lastEmployeeSelector = createSelector(
  employeesByPageSelector,
  employeesByPage => (employeesByPage[9] ? employeesByPage[9] : null)
);

export const firstEmployeeSelector = createSelector(
  employeesByPageSelector,
  employeesByPage => (employeesByPage[0] ? employeesByPage[0] : null)
);
