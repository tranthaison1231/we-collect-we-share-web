import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const EmployeesTypes = makeConstantCreator(
  'GET_DEFAULT_EMPLOYEES',
  'GET_EMPLOYEES_SUCCESS',
  'GET_EMPLOYEES_FAIL',
  'GET_NEXT_EMPLOYEES',
  'GET_PREVIOUS_EMPLOYEES',
  'CREATE_EMPLOYEE',
  'CREATE_EMPLOYEE_SUCCESS',
  'CREATE_EMPLOYEE_FAIL',
  'DELETE_EMPLOYEE',
  'DELETE_EMPLOYEE_SUCCESS',
  'DELETE_EMPLOYEE_FAIL',
  'EDIT_EMPLOYEE',
  'EDIT_EMPLOYEE_SUCCESS',
  'EDIT_EMPLOYEE_FAIL',
  'UNSUBSCRIBE'
);

export const createEmployeeAction = ({ employee }) =>
  makeActionCreator(EmployeesTypes.CREATE_EMPLOYEE, { employee });

export const createEmployeeSuccessAction = () =>
  makeActionCreator(EmployeesTypes.CREATE_EMPLOYEE_SUCCESS);

export const createEmployeeFailAction = ({ error }) =>
  makeActionCreator(EmployeesTypes.CREATE_EMPLOYEE_FAIL, { error });

export const deleteEmployeeAction = ({ id }) =>
  makeActionCreator(EmployeesTypes.DELETE_EMPLOYEE, { id });

export const deleteEmployeeSuccessAction = ({ id }) =>
  makeActionCreator(EmployeesTypes.DELETE_EMPLOYEE_SUCCESS, { id });

export const deleteEmployeeFailAction = ({ error }) =>
  makeActionCreator(EmployeesTypes.DELETE_EMPLOYEE_FAIL, { error });

export const editEmployeeAction = ({ id, employee }) =>
  makeActionCreator(EmployeesTypes.EDIT_EMPLOYEE, { id, employee });

export const editEmployeeSuccessAction = ({ id, employee }) =>
  makeActionCreator(EmployeesTypes.EDIT_EMPLOYEE_SUCCESS, { id, employee });

export const editEmployeeFailAction = ({ error }) =>
  makeActionCreator(EmployeesTypes.EDIT_EMPLOYEE_FAIL, { error });

export const getDefaultEmployeesAction = () =>
  makeActionCreator(EmployeesTypes.GET_DEFAULT_EMPLOYEES);

export const getEmployeesSuccessAction = ({ employeesByPage }) =>
  makeActionCreator(EmployeesTypes.GET_EMPLOYEES_SUCCESS, { employeesByPage });

export const getEmployeesFailAction = ({ error }) =>
  makeActionCreator(EmployeesTypes.GET_EMPLOYEES_FAIL, { error });

export const getNextEmployeesAction = ({ lastEmployee }) =>
  makeActionCreator(EmployeesTypes.GET_NEXT_EMPLOYEES, { lastEmployee });

export const getPreviousEmployeesAction = ({ firstEmployee }) =>
  makeActionCreator(EmployeesTypes.GET_PREVIOUS_EMPLOYEES, { firstEmployee });

export const unSubscribeAction = () =>
  makeActionCreator(EmployeesTypes.UNSUBSCRIBE);
