import { connectRouter } from 'connected-react-router';
import { auth } from './auth/reducer';
import modal from './modal/reducer';
import loading from './loading/reducer';
import pagingButtons from './pagingButtons/reducer';
import users from './users/reducer';
import searchBar from './searchBar/reducer';
import managerSource from './managerSource/reducer';
import contact from './contact/reducer';
import appointments from './appointments/reducer';
import positions from './positions/reducer';
import employees from './employees/reducer';

export default history => ({
  router: connectRouter(history),
  auth,
  loading,
  modal,
  users,
  managerSource,
  contact,
  pagingButtons,
  searchBar,
  appointments,
  positions,
  employees
});
