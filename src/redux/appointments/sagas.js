import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  AppointmentsTypes,
  createAppointmentFailAction,
  deleteAppointmentFailAction,
  editAppointmentFailAction,
  getAppointmentsFailAction,
  getAppointmentsSuccessAction,
  deleteAppointmentSuccessAction,
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction,
} from '../pagingButtons/actions';

function* createAppointmentSaga({ appointment }) {
  try {
    yield firebase
      .firestore()
      .collection('appointments')
      .add(appointment);
  } catch (e) {
    yield put(createAppointmentFailAction(e));
  }
}

function* editAppointmentSaga({ id, appointment }) {
  try {
    yield firebase
      .firestore()
      .collection('appointments')
      .doc(id)
      .update(appointment);
  } catch (e) {
    yield put(editAppointmentFailAction(e));
  }
}

function* deleteAppointmentSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('appointments')
      .doc(id)
      .delete();
    yield put(deleteAppointmentSuccessAction({ id }));
  } catch (e) {
    yield put(deleteAppointmentFailAction(e));
  }
}

let channel = null;

function* getDefaultAppointmentsSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('appointments')
        .orderBy('title')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(
        getAppointmentsSuccessAction({ appointmentsByPage: responseData }),
      );
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getAppointmentsFailAction(e));
  }
}

function* getNextAppointmentsSaga({ lastAppointment }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('appointments')
      .orderBy('title')
      .startAfter(lastAppointment.title)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('appointments')
          .orderBy('title')
          .startAfter(lastAppointment.title)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(
          getAppointmentsSuccessAction({ appointmentsByPage: responseData }),
        );
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getAppointmentsFailAction(e));
  }
}

function* getPreviousAppointmentsSaga({ firstAppointment }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('appointments')
      .orderBy('title', 'desc')
      .startAfter(firstAppointment.title)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('appointments')
          .orderBy('title')
          .startAt(response.docs[9].data().title)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(
          getAppointmentsSuccessAction({ appointmentsByPage: responseData }),
        );
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getAppointmentsFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(AppointmentsTypes.CREATE_APPOINTMENT, createAppointmentSaga),
  takeEvery(AppointmentsTypes.DELETE_APPOINTMENT, deleteAppointmentSaga),
  takeEvery(AppointmentsTypes.EDIT_APPOINTMENT, editAppointmentSaga),
  takeEvery(
    AppointmentsTypes.GET_DEFAULT_APPOINTMENTS,
    getDefaultAppointmentsSaga,
  ),
  takeEvery(AppointmentsTypes.GET_NEXT_APPOINTMENTS, getNextAppointmentsSaga),
  takeEvery(
    AppointmentsTypes.GET_PREVIOUS_APPOINTMENTS,
    getPreviousAppointmentsSaga,
  ),
  takeEvery(AppointmentsTypes.UNSUBSCRIBE, unSubscribeSaga),
];
