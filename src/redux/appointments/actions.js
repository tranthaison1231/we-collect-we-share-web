import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const AppointmentsTypes = makeConstantCreator(
  'GET_DEFAULT_APPOINTMENTS',
  'GET_APPOINTMENTS_SUCCESS',
  'GET_APPOINTMENTS_FAIL',
  'GET_NEXT_APPOINTMENTS',
  'GET_PREVIOUS_APPOINTMENTS',
  'CREATE_APPOINTMENT',
  'CREATE_APPOINTMENT_SUCCESS',
  'CREATE_APPOINTMENT_FAIL',
  'DELETE_APPOINTMENT',
  'DELETE_APPOINTMENT_SUCCESS',
  'DELETE_APPOINTMENT_FAIL',
  'EDIT_APPOINTMENT',
  'EDIT_APPOINTMENT_SUCCESS',
  'EDIT_APPOINTMENT_FAIL',
  'UNSUBSCRIBE',
);

export const createAppointmentAction = ({ appointment }) =>
  makeActionCreator(AppointmentsTypes.CREATE_APPOINTMENT, { appointment });

export const createAppointmentSuccessAction = () =>
  makeActionCreator(AppointmentsTypes.CREATE_APPOINTMENT_SUCCESS);

export const createAppointmentFailAction = ({ error }) =>
  makeActionCreator(AppointmentsTypes.CREATE_APPOINTMENT_FAIL, { error });

export const deleteAppointmentAction = ({ id }) =>
  makeActionCreator(AppointmentsTypes.DELETE_APPOINTMENT, { id });

export const deleteAppointmentSuccessAction = ({ id }) =>
  makeActionCreator(AppointmentsTypes.DELETE_APPOINTMENT_SUCCESS, { id });

export const deleteAppointmentFailAction = ({ error }) =>
  makeActionCreator(AppointmentsTypes.DELETE_APPOINTMENT_FAIL, { error });

export const editAppointmentAction = ({ id, appointment }) =>
  makeActionCreator(AppointmentsTypes.EDIT_APPOINTMENT, { id, appointment });

export const editAppointmentSuccessAction = ({ id, appointment }) =>
  makeActionCreator(AppointmentsTypes.EDIT_APPOINTMENT_SUCCESS, { id, appointment });

export const editAppointmentFailAction = ({ error }) =>
  makeActionCreator(AppointmentsTypes.EDIT_APPOINTMENT_FAIL, { error });

export const getDefaultAppointmentsAction = () =>
  makeActionCreator(AppointmentsTypes.GET_DEFAULT_APPOINTMENTS);

export const getAppointmentsSuccessAction = ({ appointmentsByPage }) =>
  makeActionCreator(AppointmentsTypes.GET_APPOINTMENTS_SUCCESS, { appointmentsByPage });

export const getAppointmentsFailAction = ({ error }) =>
  makeActionCreator(AppointmentsTypes.GET_APPOINTMENTS_FAIL, { error });

export const getNextAppointmentsAction = ({ lastAppointment }) =>
  makeActionCreator(AppointmentsTypes.GET_NEXT_APPOINTMENTS, { lastAppointment });

export const getPreviousAppointmentsAction = ({ firstAppointment }) =>
  makeActionCreator(AppointmentsTypes.GET_PREVIOUS_APPOINTMENTS, { firstAppointment });

export const unSubscribeAction = () =>
  makeActionCreator(AppointmentsTypes.UNSUBSCRIBE);
