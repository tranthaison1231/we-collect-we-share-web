import { ModalTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

export const initialState = {
  modals: [],
};

const showModal = (state, { data }) => ({
  ...state,
  modals: [
    ...state.modals,
    {
      id: Date.now(),
      current: data,
      isHidden: false,
      draft: {},
      item: null,
    },
  ],
});

const closeModal = (state, { id }) => ({
  ...state,
  draft: {},
  modals: state.modals.filter(modal => modal.id !== id),
});

const showModalWithItem = (state, action) => ({
  ...state,
  modals: [
    ...state.modals,
    {
      id: Date.now(),
      current: action.route,
      isHidden: false,
      draft: {},
      item: action.item,
    },
  ],
});

const toggleModal = (state, { id }) => ({
  ...state,
  modals: state.modals.map(modal =>
    id === modal.id ? { ...modal, isHidden: !modal.isHidden } : modal,
  ),
});

const saveDraft = (state, { data, id }) => ({
  ...state,
  modals: state.modals.map(modal =>
    id === modal.id ? { ...modal, draft: data } : modal,
  ),
});

export default makeReducerCreator(initialState, {
  [ModalTypes.SHOW_MODAL]: showModal,
  [ModalTypes.CLOSE_MODAL]: closeModal,
  [ModalTypes.SHOW_MODAL_WITH_ITEM]: showModalWithItem,
  [ModalTypes.TOGGLE_MODAL]: toggleModal,
  [ModalTypes.SAVE_DRAFT]: saveDraft,
});
