import { takeEvery, put, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import {
  PositionsTypes,
  createPositionFailAction,
  deletePositionFailAction,
  editPositionFailAction,
  getPositionsFailAction,
  getPositionsSuccessAction,
  deletePositionSuccessAction,
  getAllPositionsSuccessAction
} from './actions';
import {
  disableNextButtonAction,
  disablePrevButtonAction,
  enableNextButtonAction,
  enablePrevButtonAction,
  defaultButtonsAction
} from '../pagingButtons/actions';

function* createPositionSaga({ position }) {
  try {
    yield firebase
      .firestore()
      .collection('positions')
      .add(position);
  } catch (e) {
    yield put(createPositionFailAction(e));
  }
}

function* editPositionSaga({ id, position }) {
  try {
    yield firebase
      .firestore()
      .collection('positions')
      .doc(id)
      .update(position);
  } catch (e) {
    yield put(editPositionFailAction(e));
  }
}

function* deletePositionSaga({ id }) {
  try {
    yield firebase
      .firestore()
      .collection('positions')
      .doc(id)
      .delete();
    yield put(deletePositionSuccessAction({ id }));
  } catch (e) {
    yield put(deletePositionFailAction(e));
  }
}

let channel = null;

function* getDefaultPositionsSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('positions')
        .orderBy('name')
        .limit(10)
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getPositionsSuccessAction({ positionsByPage: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getPositionsFailAction(e));
  }
}

function* getAllPositionsSaga() {
  try {
    if (channel !== null) {
      channel.close();
    }
    channel = eventChannel(emit => {
      const listener = firebase
        .firestore()
        .collection('positions')
        .orderBy('name')
        .onSnapshot(docs => {
          emit(docs);
        });
      return () => {
        listener();
      };
    });
    while (true) {
      const { docs } = yield take(channel);
      const responseData = [];
      docs.forEach(doc => {
        responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
      });
      yield put(getAllPositionsSuccessAction({ positions: responseData }));
      yield put(defaultButtonsAction());
    }
  } catch (e) {
    yield put(getPositionsFailAction(e));
  }
}

function* getNextPositionsSaga({ lastPosition }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('positions')
      .orderBy('name')
      .startAfter(lastPosition.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('positions')
          .orderBy('name')
          .startAfter(lastPosition.name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getPositionsSuccessAction({ positionsByPage: responseData }));
        yield put(enablePrevButtonAction());
      }
    } else {
      yield put(disableNextButtonAction());
    }
  } catch (e) {
    yield put(getPositionsFailAction(e));
  }
}

function* getPreviousPositionsSaga({ firstPosition }) {
  try {
    const response = yield firebase
      .firestore()
      .collection('positions')
      .orderBy('name', 'desc')
      .startAfter(firstPosition.name)
      .limit(10)
      .get();
    if (response.docs.length > 0) {
      channel.close();
      channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('positions')
          .orderBy('name')
          .startAt(response.docs[9].data().name)
          .limit(10)
          .onSnapshot(docs => {
            emit(docs);
          });
        return () => {
          listener();
        };
      });
      while (true) {
        const { docs } = yield take(channel);
        const responseData = [];
        docs.forEach(doc => {
          responseData.push({ ...doc.data(), id: doc.id, key: doc.id });
        });
        yield put(getPositionsSuccessAction({ positionsByPage: responseData }));
        yield put(enableNextButtonAction());
      }
    } else {
      yield put(disablePrevButtonAction());
    }
  } catch (e) {
    yield put(getPositionsFailAction(e));
  }
}

function unSubscribeSaga() {
  if (channel !== null) channel.close();
}

export default [
  takeEvery(PositionsTypes.CREATE_POSITION, createPositionSaga),
  takeEvery(PositionsTypes.DELETE_POSITION, deletePositionSaga),
  takeEvery(PositionsTypes.EDIT_POSITION, editPositionSaga),
  takeEvery(PositionsTypes.GET_DEFAULT_POSITIONS, getDefaultPositionsSaga),
  takeEvery(PositionsTypes.GET_NEXT_POSITIONS, getNextPositionsSaga),
  takeEvery(PositionsTypes.GET_PREVIOUS_POSITIONS, getPreviousPositionsSaga),
  takeEvery(PositionsTypes.UNSUBSCRIBE, unSubscribeSaga),
  takeEvery(PositionsTypes.GET_ALL_POSITIONS, getAllPositionsSaga)
];
