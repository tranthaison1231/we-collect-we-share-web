import { createSelector } from 'reselect';

const contactsByPageSelector = state => state.contact.contacts;

export const lastContactSelector = createSelector(
  contactsByPageSelector,
  contactsByPage => (contactsByPage[9] ? contactsByPage[9] : null)
);

export const firstContactSelector = createSelector(
  contactsByPageSelector,
  contactsByPage => (contactsByPage[0] ? contactsByPage[0] : null)
);
