import { takeEvery, put, call, take } from 'redux-saga/effects';
import { eventChannel } from 'redux-saga';
import firebase from 'firebase';
import { onAuthStateChanged } from '../../firebase';
import {
  loginFirebaseAction,
  logoutFirebaseAction,
  getCurrentUserFirebaseAction,
  loginSuccessAction,
  loginFailureAction,
  getCurentUserSuccessAction,
  logoutAction,
  getCurentUserFailureAction
} from './actions';

function* loginFirebaseSaga({ payload }) {
  try {
    const result = yield firebase.auth().signInWithPopup(payload);
    const sessionToken = result.credential.accessToken;
    let idToken = yield result.user.getIdTokenResult(true);
    if (idToken.claims.admin) {
      localStorage.setItem('sessionToken', sessionToken);
      yield put(loginSuccessAction(result.user));
      localStorage.setItem('fullName', result.user.displayName);
      localStorage.setItem('id', result.user.uid);
      yield put(
        getCurentUserSuccessAction({
          user: result.user,
          admin: idToken.claims.admin
        })
      );
    } else {
      const channel = eventChannel(emit => {
        const listener = firebase
          .firestore()
          .collection('metadata')
          .onSnapshot(snapshot => {
            snapshot.docChanges().forEach(change => {
              if (change.type === 'added') {
                if (change.doc.id === result.user.uid) {
                  emit(true);
                }
              }
            });
            setTimeout(() => emit(false), 3000);
          });
        return () => {
          listener();
        };
      });
      const isDocChanged = yield take(channel);
      channel.close();
      if (isDocChanged) {
        idToken = yield result.user.getIdTokenResult(true);
        if (idToken.claims.admin) {
          localStorage.setItem('sessionToken', sessionToken);
          yield put(loginSuccessAction(result.user));
          localStorage.setItem('fullName', result.user.displayName);
          localStorage.setItem('id', result.user.uid);
          yield put(
            getCurentUserSuccessAction(result.user, idToken.claims.admin)
          );
        } else {
          yield put(logoutFirebaseAction());
        }
      } else {
        yield put(logoutFirebaseAction());
      }
    }
  } catch (e) {
    console.error(e.message);
    yield put(loginFailureAction(e));
  }
}

function* logoutFirebaseSaga() {
  try {
    yield firebase.auth().signOut();
    localStorage.clear('sessionToken');
    localStorage.clear('fullName');
    localStorage.clear('id');
    yield put(logoutAction());
  } catch (e) {
    console.error(e.message);
  }
}

function* getCurrentUserFirebaseSaga() {
  try {
    const user = yield call(onAuthStateChanged);
    const idToken = yield user.getIdTokenResult(true);
    yield put(
      getCurentUserSuccessAction({ user, admin: idToken.claims.admin })
    );
  } catch (e) {
    console.error(e.message);
    yield put(getCurentUserFailureAction(e));
  }
}

export default [
  takeEvery(loginFirebaseAction.type, loginFirebaseSaga),
  takeEvery(logoutFirebaseAction.type, logoutFirebaseSaga),
  takeEvery(getCurrentUserFirebaseAction.type, getCurrentUserFirebaseSaga)
];
