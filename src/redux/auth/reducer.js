import { createReducer } from 'redux-starter-kit';
import {
  loginSuccessAction,
  loginFailureAction,
  logoutAction,
  getCurentUserSuccessAction,
  getCurentUserFailureAction,
  updateUserAction,
  updateUserSuccessAction,
  updateUserFailureAction,
  forgotPasswordAction,
  forgotPasswordSuccessAction,
  forgotPasswordFailureAction,
  resetPasswordSuccessAction,
  resetPasswordFailureAction
} from './actions';

export const initialState = {
  isAuthenticated: !!localStorage.getItem('sessionToken'),
  data: {},
  roles: false,
  loginError: false,
  loginSuccess: false
};

const loginSuccess = (state, action) => ({
  ...state,
  isAuthenticated: true,
  loginError: false,
  loginSuccess: true,
  data: action.data
});

const loginFail = (state, action) => ({
  ...state,
  isAuthenticated: false,
  loginError: action.error,
  loginSuccess: false
});

const logout = () => ({ ...initialState, isAuthenticated: false, data: {} });

const getCurentUserSuccess = (state, action) => ({
  ...state,
  data: action.payload.user,
  roles: action.payload.admin
});

const getCurentUserFailure = (state, { error }) => ({
  ...state,
  error
});

const updateUser = state => ({
  ...state,
  loading: true
});

const updateUserSuccess = (state, action) => ({
  ...state,
  data: {
    ...state.data,
    ...action.data
  },
  loading: false
});

const updateUserFailure = state => ({
  ...state,
  loading: false
});

const forgotPassword = state => ({
  ...state,
  loading: true
});

const forgotPasswordSuccess = state => ({
  ...state,
  loading: false
});

const forgotPasswordFailure = (state, { error }) => ({
  ...state,
  loading: false,
  error
});

const resetPasswordSuccess = state => ({
  ...state,
  loading: false
});

const resetPasswordFailure = (state, { error }) => ({
  ...state,
  loading: false,
  error
});

export const auth = createReducer(initialState, {
  [loginSuccessAction.type]: loginSuccess,
  [loginFailureAction.type]: loginFail,
  [logoutAction.type]: logout,

  [getCurentUserSuccessAction.type]: getCurentUserSuccess,
  [getCurentUserFailureAction.type]: getCurentUserFailure,

  [updateUserAction.type]: updateUser,
  [updateUserSuccessAction.type]: updateUserSuccess,
  [updateUserFailureAction.type]: updateUserFailure,

  [forgotPasswordAction.type]: forgotPassword,
  [forgotPasswordSuccessAction.type]: forgotPasswordSuccess,
  [forgotPasswordFailureAction.type]: forgotPasswordFailure,

  [resetPasswordSuccessAction.type]: resetPasswordSuccess,
  [resetPasswordFailureAction.type]: resetPasswordFailure
});
