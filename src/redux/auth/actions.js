import { createAction } from 'redux-starter-kit';

export const loginFirebaseAction = createAction('LOGIN_FIREBASE');
export const loginSuccessAction = createAction('LOGIN_AUTH_SUCCESS');
export const loginFailureAction = createAction('LOGIN_AUTH_FAIL');
export const logoutAction = createAction('LOGOUT');
export const logoutFirebaseAction = createAction('LOGOUT_FIREBASE');
export const getCurrentUserFirebaseAction = createAction(
  'GET_CURRENT_USER_FIREBASE'
);

export const getCurentUserSuccessAction = createAction(
  'GET_CURRENT_USER_SUCCESS'
);
export const getCurentUserFailureAction = createAction(
  'GET_CURRENT_USER_FAILURE'
);
export const updateUserAction = createAction('UPDATE_USER');
export const updateUserSuccessAction = createAction('UPDATE_USER_SUCCESS');
export const updateUserFailureAction = createAction('UPDATE_USER_FAILURE');

export const forgotPasswordAction = createAction('FORGOT_PASSWORD');
export const forgotPasswordSuccessAction = createAction(
  'FORGOT_PASSWORD_SUCCESS'
);
export const forgotPasswordFailureAction = createAction(
  'FORGOT_PASSWORD_FAILURE'
);

export const resetPasswordSuccessAction = createAction('REGISTER_SUCCESS');
export const resetPasswordFailureAction = createAction('REGISTER_FAILURE');
