import { all } from 'redux-saga/effects';
import authSaga from './auth/sagas';
import usersSaga from './users/sagas';
import employeesSaga from './employees/sagas';
import firebaseImg from './firebaseImg/sagas';

export default function* root() {
  yield all([...authSaga, ...usersSaga, ...employeesSaga, ...firebaseImg]);
}
