import { LoadingTypes } from './actions';
import { makeReducerCreator } from '../../utils/reduxUtils';

const initialState = {
  isLoading: false,
};

export const changeLoading = (state, { data }) => ({
  ...state,
  isLoading: data,
});
export default makeReducerCreator(initialState, {
  [LoadingTypes.CHANGE_LOADING]: changeLoading,
});
