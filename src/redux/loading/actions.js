import { makeConstantCreator, makeActionCreator } from '../../utils/reduxUtils';

export const LoadingTypes = makeConstantCreator('CHANGE_LOADING');

export const changeLoading = data =>
  makeActionCreator(LoadingTypes.CHANGE_LOADING, {
    data,
  });
