import { makeReducerCreator } from '../../utils/reduxUtils';
import { UsersTypes } from './actions';

export const initialState = {
  usersByPage: [],
  error: null,
};

const createUserSuccess = state => ({
  ...state,
  error: null,
});

const createUserFail = (state, action) => ({
  ...state,
  error: action.error,
});

const deleteUserSuccess = (state, action) => ({
  ...state,
  error: null,
  usersByPage: state.usersByPage.filter(user => user.id !== action.id),
});

const deleteUserFail = (state, action) => ({
  ...state,
  error: action.error,
});

const editUserSuccess = (state, action) => ({
  ...state,
  error: null,
  usersByPage: state.usersByPage.map(user => (user.id === action.id) ? {...user, role: action.user.role} : user),
});

const editUserFail = (state, action) => ({
  ...state,
  error: action.error,
});

const getUsersSuccess = (state, action) => ({
  ...state,
  usersByPage: action.usersByPage,
});

const getUsersFail = (state, action) => ({
  ...state,
  error: action.error,
});

const users = makeReducerCreator(initialState, {
  [UsersTypes.CREATE_USER_FAIL]: createUserFail,
  [UsersTypes.CREATE_USER_SUCCESS]: createUserSuccess,
  [UsersTypes.DELETE_USER_FAIL]: deleteUserFail,
  [UsersTypes.DELETE_USER_SUCCESS]: deleteUserSuccess,
  [UsersTypes.EDIT_USER_FAIL]: editUserFail,
  [UsersTypes.EDIT_USER_SUCCESS]: editUserSuccess,
  [UsersTypes.GET_USERS_FAIL]: getUsersFail,
  [UsersTypes.GET_USERS_SUCCESS]: getUsersSuccess,
});

export default users;
