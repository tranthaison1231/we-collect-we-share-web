import { createSelector } from 'reselect';

const usersByPageSelector = state => state.users.usersByPage;

export const lastUserSelector = createSelector(
  usersByPageSelector,
  usersByPage => (usersByPage[9] ? usersByPage[9] : null)
);

export const firstUserSelector = createSelector(
  usersByPageSelector,
  usersByPage => (usersByPage[0] ? usersByPage[0] : null)
);
