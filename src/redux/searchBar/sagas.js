import { takeEvery, put } from 'redux-saga/effects';
import { SearchBarTypes, changeSearchResultsAction } from './actions';

function* algoliaSearchSaga({ index, query }) {
  try {
    const { hits } = yield index.search({ query });
    yield put(changeSearchResultsAction(hits));
  } catch (err) {
    console.error(err.message);
  }
}

export default [takeEvery(SearchBarTypes.ALGOLIA_SEARCH, algoliaSearchSaga)];
