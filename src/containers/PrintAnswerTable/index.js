import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AnswerPage from '../../components/AnswerForm';

const PrintAnswer = ({ match, getAnswerById, answer }) => {
  useEffect(() => {
    getAnswerById(match.params.id);
  }, [getAnswerById, match.params.id]);
  return (
    <>
      <AnswerPage {...{ answer }} />
    </>
  );
};

PrintAnswer.propTypes = {
  match: PropTypes.object.isRequired,
  getAnswerById: PropTypes.func,
  answer: PropTypes.object
};

export default connect(
  state => ({
    answer: state.answers.answer
  }),
)(PrintAnswer);
