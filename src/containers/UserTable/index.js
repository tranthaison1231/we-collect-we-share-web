import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Icon, Dropdown, Menu, Avatar } from 'antd';
import {
  deleteUserAction,
  getDefaultUsersAction,
  getNextUsersAction,
  getPreviousUsersAction,
  unSubscribeAction,
  getFilteredUsersAction,
} from '../../redux/users/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import {
  lastUserSelector,
  firstUserSelector,
} from '../../redux/users/selector';
import CustomTable from '../../components/common/CustomTable';
import SearchBar from '../../components/common/SearchBar';
import { algoliaSearchAction } from '../../redux/searchBar/actions';

const DashboardContainer = ({
  usersByPage,
  showModalWithItem,
  deleteUser,
  getDefaultUsers,
  getPreviousUsers,
  getNextUsers,
  lastUser,
  firstUser,
  disableNext,
  disablePrev,
  unSubscribe,
  getFilteredUsers,
  algoliaSearch,
  results,
}) => {
  useEffect(() => {
    getDefaultUsers();
    return () => {
      unSubscribe();
    };
  }, [getDefaultUsers, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    if (usersByPage.length === 1) getPreviousUsers({ firstUser });
    deleteUser({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: i18n.t('table.title.user'),
      dataIndex: 'email',
      key: 'email',
      render: (text, row) => (
        <span>
          {row.user ? (
            <Avatar src={row.user.photoURL} />
          ) : (
            <Avatar icon="clock-circle" />
          )}
          <span className="email-field">{text}</span>
        </span>
      ),
    },
    {
      title: i18n.t('table.title.role'),
      dataIndex: 'role',
      key: 'role',
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstUser}
        lastData={lastUser}
        disableNext={disableNext}
        disablePrev={disablePrev}
        columns={columns}
        getPreviousData={() => getPreviousUsers({ firstUser })}
        getNextData={() => getNextUsers({ lastUser })}
        dataSource={usersByPage}
      />
      <SearchBar
        algoliaSearch={algoliaSearch}
        results={results}
        onCancel={getDefaultUsers}
        getFilteredData={getFilteredUsers}
      />
    </>
  );
};

DashboardContainer.propTypes = {
  usersByPage: PropTypes.array,
  deleteUser: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getDefaultUsers: PropTypes.func,
  getPreviousUsers: PropTypes.func,
  getNextUsers: PropTypes.func,
  lastUser: PropTypes.object,
  firstUser: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  unSubscribe: PropTypes.func,
  getFilteredUsers: PropTypes.func,
  results: PropTypes.array,
  algoliaSearch: PropTypes.func,
};

const mapStateToProps = state => ({
  usersByPage: state.users.usersByPage,
  lastUser: lastUserSelector(state),
  firstUser: firstUserSelector(state),
  disableNext: state.pagingButtons.disableNext,
  disablePrev: state.pagingButtons.disablePrev,
  results: state.searchBar.results,
});

const mapDispatchToProps = dispatch => ({
  deleteUser: params => dispatch(deleteUserAction(params)),
  showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  getDefaultUsers: () => dispatch(getDefaultUsersAction()),
  getNextUsers: params => dispatch(getNextUsersAction(params)),
  getPreviousUsers: params => dispatch(getPreviousUsersAction(params)),
  unSubscribe: () => dispatch(unSubscribeAction()),
  getFilteredUsers: keyword => dispatch(getFilteredUsersAction(keyword)),
  algoliaSearch: options => dispatch(algoliaSearchAction(options)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardContainer);
