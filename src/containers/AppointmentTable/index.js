import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import i18n from 'i18next';
import { Icon, Dropdown, Menu } from 'antd';
import {
  deleteAppointmentAction,
  getDefaultAppointmentsAction,
  getNextAppointmentsAction,
  getPreviousAppointmentsAction,
  unSubscribeAction,
} from '../../redux/appointments/actions';
import { showModalWithItemAction } from '../../redux/modal/actions';
import {
  lastAppointmentSelector,
  firstAppointmentSelector,
} from '../../redux/appointments/selectors';
import CustomTable from '../../components/common/CustomTable';

const AppointmentTable = ({
  appointmentsByPage,
  showModalWithItem,
  deleteAppointment,
  getDefaultAppointments,
  getPreviousAppointments,
  getNextAppointments,
  lastAppointment,
  firstAppointment,
  disableNext,
  disablePrev,
  unSubscribe,
}) => {
  useEffect(() => {
    getDefaultAppointments();
    return () => {
      unSubscribe();
    };
  }, [getDefaultAppointments, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/appointments/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDele = record => {
    deleteAppointment({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDele,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Time',
      dataIndex: 'startTime',
      render: (startTime, row) => (
        <span>
          From {new Date(startTime).toLocaleString()}
          <br />
          To {new Date(row.endTime).toLocaleString()}
        </span>
      ),
      key: 'startTime',
    },
    {
      title: 'Location',
      dataIndex: 'location',
      key: 'location',
    },
    {
      title: 'Attendees',
      dataIndex: 'attendees',
      key: 'attendees',
      render: record =>
        Array.isArray(record) &&
        record.map(item => (
          <span key={item}>
            {`${item} `}
            <br />
          </span>
        )),
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    <>
      <CustomTable
        firstData={firstAppointment}
        lastData={lastAppointment}
        disableNext={disableNext}
        disablePrev={disablePrev}
        columns={columns}
        getPreviousData={() => getPreviousAppointments({ firstAppointment })}
        getNextData={() => getNextAppointments({ lastAppointment })}
        dataSource={appointmentsByPage}
      />
    </>
  );
};

AppointmentTable.propTypes = {
  appointmentsByPage: PropTypes.array,
  deleteAppointment: PropTypes.func,
  showModalWithItem: PropTypes.func,
  getDefaultAppointments: PropTypes.func,
  getPreviousAppointments: PropTypes.func,
  getNextAppointments: PropTypes.func,
  lastAppointment: PropTypes.object,
  firstAppointment: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  unSubscribe: PropTypes.func,
};

export default connect(
  state => ({
    appointmentsByPage: state.appointments.appointmentsByPage,
    lastAppointment: lastAppointmentSelector(state),
    firstAppointment: firstAppointmentSelector(state),
    disableNext: state.pagingButtons.disableNext,
    disablePrev: state.pagingButtons.disablePrev,
  }),
  dispatch => ({
    deleteAppointment: params => dispatch(deleteAppointmentAction(params)),
    showModalWithItem: params => dispatch(showModalWithItemAction(params)),
    getDefaultAppointments: () => dispatch(getDefaultAppointmentsAction()),
    getNextAppointments: params => dispatch(getNextAppointmentsAction(params)),
    getPreviousAppointments: params =>
      dispatch(getPreviousAppointmentsAction(params)),
    unSubscribe: () => dispatch(unSubscribeAction()),
  })
)(AppointmentTable);
