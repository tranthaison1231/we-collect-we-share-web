import React, { useEffect } from 'react';
import { Icon, Dropdown, Menu } from 'antd';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import i18n from 'i18next';
import {
  getAllContactAction,
  deleteContactAction,
  getNextContactsAction,
  getPrevContactsAction,
  unSubscribeAction,
} from '../../redux/contact/actions';
import {
  firstContactSelector,
  lastContactSelector,
} from '../../redux/contact/selectors';
import { showModalWithItemAction } from '../../redux/modal/actions';
import CustomTable from '../../components/common/CustomTable';

const ContactTableContainer = ({
  contacts: { contacts },
  getAllContact,
  deleteContact,
  firstContact,
  lastContact,
  disableNext,
  disablePrev,
  getNextContacts,
  getPreviousContacts,
  unSubscribe,
  showModalWithItem,
}) => {
  useEffect(() => {
    getAllContact();
    return () => {
      unSubscribe();
    };
  }, [getAllContact, unSubscribe]);
  const gotoEditPage = record => {
    const route = '/contact/edit';
    showModalWithItem({ route, item: record });
  };
  const handleDelete = record => {
    deleteContact({ id: record.id });
  };
  const menuItems = [
    {
      id: 0,
      text: i18n.t('button.delete'),
      handler: handleDelete,
    },
    {
      id: 1,
      text: i18n.t('button.edit'),
      handler: gotoEditPage,
    },
  ];
  const columns = [
    {
      title: 'Name',
      dataIndex: 'data.name',
      key: 'data.name',
    },
    {
      title: 'Years',
      dataIndex: 'data.age',
      key: 'data.age',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.data.age - b.data.age,
    },
    {
      title: 'Email',
      dataIndex: 'data.email',
      key: 'data.email',
    },
    {
      title: 'Phone',
      dataIndex: 'data.phone',
      key: 'data.phone',
    },
    {
      title: 'Company/University',
      dataIndex: 'data.company',
      key: 'data.company',
    },
    {
      title: 'Position',
      dataIndex: 'data.position',
      key: 'data.position',
      render: record =>
        Array.isArray(record) &&
        record.map(item => <span key={item}>{`${item} `}</span>),
    },
    {
      title: '',
      dataIndex: '',
      key: 'action',
      render: record => (
        <Dropdown
          placement="bottomCenter"
          overlay={
            <Menu>
              {menuItems.map(menuItem => (
                <Menu.Item
                  key={menuItem.id}
                  onClick={() => menuItem.handler(record)}
                >
                  {menuItem.text}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Icon type="more" />
        </Dropdown>
      ),
    },
  ];
  return (
    contacts && (
      <>
        <CustomTable
          firstData={firstContact}
          lastData={lastContact}
          disableNext={disableNext}
          disablePrev={disablePrev}
          columns={columns}
          getPreviousData={() => getPreviousContacts({ firstContact })}
          getNextData={() => getNextContacts({ lastContact })}
          dataSource={contacts}
          rowKey={record => record.id}
        />
      </>
    )
  );
};

ContactTableContainer.propTypes = {
  contact: PropTypes.object,
  getAllContact: PropTypes.func,
  deleteContact: PropTypes.func,
  firstContact: PropTypes.object,
  lastContact: PropTypes.object,
  disableNext: PropTypes.bool,
  disablePrev: PropTypes.bool,
  getNextContacts: PropTypes.func,
  getPreviousContacts: PropTypes.func,
  unSubscribe: PropTypes.func,
  showModalWithItem: PropTypes.func,
};

export default connect(
  state => ({
    contacts: state.contact,
    firstContact: firstContactSelector(state),
    lastContact: lastContactSelector(state),
    disableNext: state.pagingButtons.disableNext,
    disablePrev: state.pagingButtons.disablePrev,
  }),
  dispatch => ({
    getAllContact: () => dispatch(getAllContactAction()),
    deleteContact: params => dispatch(deleteContactAction(params)),
    getNextContacts: params => dispatch(getNextContactsAction(params)),
    getPreviousContacts: params => dispatch(getPrevContactsAction(params)),
    unSubscribe: () => dispatch(unSubscribeAction()),
    showModalWithItem: params => dispatch(showModalWithItemAction(params)),
  })
)(ContactTableContainer);
