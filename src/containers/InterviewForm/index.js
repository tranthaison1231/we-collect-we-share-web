import React, { createRef } from 'react';
import PropTypes from 'prop-types';
import { getPreviewInterviewByIdAction } from '../../redux/previewInterview/actions';
import { Header, Footer, FormInterview } from '../../components/InterviewForm';
import { withFetchDataById } from '../../hocs';

const InterviewForm = ({ data: previewInterview }) => {
  const arrRef = [];
  if (previewInterview) {
    previewInterview.questions.map((question, index) => {
      arrRef[index] = createRef(null);
      return null;
    });
  }
  return (
    previewInterview && (
      <>
        <Header />
        <FormInterview {...{ previewInterview, arrRef }} />
        <Footer />
      </>
    )
  );
};

InterviewForm.propTypes = {
  getPreviewInterviewById: PropTypes.func.isRequired,
  previewInterview: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withFetchDataById({
  action: getPreviewInterviewByIdAction,
  state: state => state.previewInterview.previewInterview,
})(InterviewForm);
