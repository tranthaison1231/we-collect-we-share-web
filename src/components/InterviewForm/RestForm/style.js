import styled from 'styled-components';

export default styled.div`
  width: 100%;
  margin-bottom: 127px;
  display: flex;
  align-items: center;
  flex-direction: column;
  .ant-form {
    width: 68%;
  }
  .line {
    border-top: 1px solid #c1351f;
    margin-bottom: 49px;
    margin-top: 49px;
  }
  .button {
    border: 0;
    font-size: 1.125em;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    letter-spacing: 0.45px;
    color: #ffffff;
    width: 11.25em;
    height: 2.8125em;
    border-radius: 2px;
    background: #c1351f;
    position: relative;
    box-sizing: border-box;
    transition: all 200ms ease-in;
    &::after {
      content: '';
      position: absolute;
      z-index: -1;
      top: 10px;
      left: 10px;
      border: solid 2px #c1351f;
      width: 11.25em;
      height: 2.8125em;
      border-radius: 2px;
      transition: all 200ms ease-in;
      box-sizing: border-box;
    }
    &:hover,
    &:active,
    &:focus {
      background: #fff;
      color: #c1351f;
      border: solid 2px #c1351f;
      &::after {
        background: #c1351f;
      }
    }
  }
`;
