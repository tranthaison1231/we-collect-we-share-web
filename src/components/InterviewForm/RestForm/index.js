import React, { useEffect } from 'react';
import { Form, Button } from 'antd';
import PropTypes from 'prop-types';
import Question from '../Question';
import ProgressBar from '../ProgressBar';
import FormWrapper from './style';

const RestForm = ({ previewInterview, form, arrRef }) => {
  const handleSubmit = e => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };
  let i = 0;

  const isInviewPort = e => (
      e.current.offsetTop < window.scrollY + (window.innerHeight * 1) / 3 &&
      e.current.offsetTop + e.current.clientHeight >
        window.scrollY + (window.innerHeight * 1) / 3
    );

  const handleScroll = () => {
    if (arrRef.length > 0) {
      arrRef.map(e => {
        if (e.current != null) {
          if (isInviewPort(e)) {
            e.current.style.opacity = 1;
          } else {
            e.current.style.opacity = 0.16;
          }
        }
        return null;
      });
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  });
  return (
    <FormWrapper>
      <Form onSubmit={handleSubmit}>
        {previewInterview.questions.map((question, index) => {
          if (form.getFieldValue(question.data.text)) {
            const e = previewInterview.questions.length;
            i += 100 / e;
          }
          return (
            <Question
              {...{ form }}
              questionRef={arrRef[index]}
              question={question.data}
              index={index + 1}
              key={question.id}
            />
          );
        })}
        <hr className="line" />
        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className="button"
            id={`qs-${previewInterview.questions.length + 1}`}
          >
            Submit
          </Button>
        </Form.Item>
      </Form>
      <ProgressBar
        percent={Number(i.toFixed(2))}
        strokeWidth={11}
        format={percent => `${percent}% Completed`}
      />
    </FormWrapper>
  );
};

RestForm.propTypes = {
  form: PropTypes.object.isRequired,
  previewInterview: PropTypes.object.isRequired,
  arrRef: PropTypes.array.isRequired,
};

export default Form.create()(RestForm);
