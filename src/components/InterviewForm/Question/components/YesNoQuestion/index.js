import React from 'react';
import { Form, Radio } from 'antd';
import PropTypes from 'prop-types';
import YesNoQuestionWrapper from './style';

const YesNoQuestion = ({ source, form }) => (
    <YesNoQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please choose the answer',
            },
          ],
        })(
          <Radio.Group>
            <Radio value="Yes">Yes</Radio>
            <Radio value="No">No...</Radio>
          </Radio.Group>,
        )}
      </Form.Item>
    </YesNoQuestionWrapper>
  );

YesNoQuestion.propTypes = {
  form: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
};

export default YesNoQuestion;
