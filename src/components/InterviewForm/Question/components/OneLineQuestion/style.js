import styled from 'styled-components';

const formPrimaryColor = props => props.theme.palette.formPrimaryColor;
const formDarkColor = props => props.theme.palette.formDarkColor;

export default styled.div`
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customInput {
      width: 70%;
      height: 1.9em;
      margin-bottom: 0.5em;
      background-color: transparent;
      color: ${formDarkColor};
      font-size: 1.1875em;
      line-height: 38px;
      resize: none;
      padding: 0px 0px 10px 30px;
      border-width: initial;
      border-style: none;
      border-color: initial;
      border-radius: 0px;
      border-image: initial;
      transition: all 0.2s ease 0s;
      overflow: hidden;
      outline: none;
      box-shadow: #8080804f 0px 1px;
      @media all and (max-width: 768px) {
        width: 100%;
        font-size: 1.1em;
        padding: 0px 0px 10px 0px;
      }
    }
    .customInput:focus {
      box-shadow: ${formPrimaryColor} 0px 1px;
    }
    .customInput:hover {
      background-color: transparent;
    }
  }
`;
