import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import i18n from 'i18next';
import OnlineQuestionWrapper from './style';

const OnlineQuestion = ({ source, form }) => (
    <OnlineQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please fill the answer',
            },
          ],
        })(
          <Input
            className="customInput"
            placeholder={i18n.t('input.answer.placeholder')}
          />,
        )}
      </Form.Item>
    </OnlineQuestionWrapper>
  );

OnlineQuestion.propTypes = {
  source: PropTypes.string.isRequired,
  form: PropTypes.object.isRequired,
};

export default OnlineQuestion;
