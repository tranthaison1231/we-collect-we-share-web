import styled from 'styled-components';

export default styled.div`
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customTime {
      margin-left: 30px;
      width: 10em;
      height: 50px;
      .ant-time-picker-icon,
      .ant-time-picker-clear {
        top: 32%;
      }
      @media all and (max-width: 768px) {
        font-size: 1em;
        width: 10em;
        margin-left: 0px;
      }
      .ant-time-picker-input {
        color: #3a393d;
        font-size: 1.1875em;
        letter-spacing: 3px;
        @media all and (max-width: 768px) {
          font-size: 1em;
        }
        &::placeholder {
          letter-spacing: 0px;
        }
      }
      .ant-time-picker-input:hover {
        border-color: #c1351f;
      }
      .ant-time-picker-input:focus {
        border-color: #c1351f;
      }
    }
  }
`;
