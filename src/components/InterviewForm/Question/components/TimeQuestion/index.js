import React from 'react';
import PropTypes from 'prop-types';
import { Form, TimePicker } from 'antd';

import TimeQuestionWrapper from './style';

const TimeQuestion = ({ source, form }) => (
    <TimeQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please choose your time',
            },
          ],
        })(
          <TimePicker
            className="customTime"
            placeholder="HH:MM"
            format="H:m"
          />,
        )}
      </Form.Item>
    </TimeQuestionWrapper>
  );

TimeQuestion.propTypes = {
  source: PropTypes.string.isRequired,
  form: PropTypes.object.isRequired,
};

export default TimeQuestion;
