import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input } from 'antd';
import i18n from 'i18next';
import MutipleQuestionWrapper from './style';

const { TextArea } = Input;

const OnlineQuestion = ({ source, form }) => (
    <MutipleQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: i18n.t('input.answer.validateMsg.required'),
            },
          ],
        })(
          <TextArea
            className="customTextArea"
            placeholder={i18n.t('input.answer.placeholder')}
            autosize={{ minRows: 1 }}
          />,
        )}
      </Form.Item>
    </MutipleQuestionWrapper>
  );

OnlineQuestion.propTypes = {
  source: PropTypes.string.isRequired,
  form: PropTypes.object.isRequired,
};

export default OnlineQuestion;
