import React from 'react';
import PropTypes from 'prop-types';
import { Form, Radio } from 'antd';
import RateQuestionWrapper, { RadioWrapper } from './style';

const RateQuestion = ({ form, source, count }) => {
  const arr = [];
  for (let i = 1; i <= count; i++) {
    arr[i] = i;
  }
  const { getFieldDecorator } = form;
  return (
    <RateQuestionWrapper>
      <Form.Item>
        {getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Please fill the answer',
            },
          ],
        })(
          <Radio.Group>
            {arr.map((answer, index) => (
              <RadioWrapper index={index} key={answer}>
                <Radio value={answer} />
              </RadioWrapper>
            ))}
          </Radio.Group>,
        )}
      </Form.Item>
    </RateQuestionWrapper>
  );
};

RateQuestion.propTypes = {
  form: PropTypes.object.isRequired,
  source: PropTypes.string.isRequired,
  count: PropTypes.number,
};

RateQuestion.defaultProps = {
  count: 10,
};

export default RateQuestion;
