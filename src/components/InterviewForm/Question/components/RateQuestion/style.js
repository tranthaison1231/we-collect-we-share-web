import styled from 'styled-components';

const formSecondaryColor = props => props.theme.palette.formSecondaryColor;
const formPrimaryColor = props => props.theme.palette.formPrimaryColor;

export const RadioWrapper = styled.div`
  flex: 1;
  &:not(:last-child) {
    border-right:2px solid ${formSecondaryColor};
  }
  .ant-radio-inner {
    &::after {
      content: '${props => props.index}';
    }
  }
  .ant-radio-wrapper-checked .ant-radio-inner {
    &::after {
      content: '${props => props.index}';
      color: white !important;
    }
  }
`;

export default styled.div`
  .ant-col {
    width: 100%;
    height: 100%;
  }
  .ant-form-item {
    display: flex;
    width: 73%;
    @media all and (max-width: 768px) {
      width: 100%;
    }
    .customInput {
      padding: 0;
      box-shadow: #8080804f 0px 1px;
    }
    .ant-radio-group {
      display: flex;
      margin-left: 30px;
      border: solid 2px ${formSecondaryColor};
      @media all and (max-width: 768px) {
        margin-left: 0px;
        font-size: 1em;
      }
      .ant-radio-wrapper {
        display: flex;
        align-items: center;
        justify-content: center;
        margin: 0;
        border-right: 0;
        .ant-radio {
          width: 100%;
        }
        .ant-radio-inner {
          height: 4em;
          font-size: 1.5em;
          width: 100%;
          border: 0;
          border-radius: 0;
          position: relative;
          &::after {
            top: 50%;
            transform: translate(-50%, -50%);
            left: 50%;
            background-color: transparent;
            color: ${formPrimaryColor};
            opacity: 1;
          }
        }
      }
      .ant-radio-wrapper-checked .ant-radio-inner {
        background: ${formPrimaryColor};
        width: 100%;
      }
    }
    .ant-radio-wrapper:hover .ant-radio,
    .ant-radio:hover .ant-radio-inner,
    .ant-radio-input:focus + .ant-radio-inner {
      background: ${formPrimaryColor};
      &::after {
        color: white !important;
      }
    }
  }
`;
