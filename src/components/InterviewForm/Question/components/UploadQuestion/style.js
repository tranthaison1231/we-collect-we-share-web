import styled from 'styled-components';

export default styled.div`
  .ant-col {
    width: 100%;
  }
  .ant-form-item {
    display: flex;
    .customUpload {
      margin-left: 30px;
      width: 15em;
      @media all and (max-width: 768px) {
        margin-left: 0px;
        font-size: 1em;
      }
      .ant-btn:focus,
      .ant-btn:hover,
      .ant-btn:active {
        border-color: #c1351f;
        color: #c1351f;
      }
    }
  }
`;
