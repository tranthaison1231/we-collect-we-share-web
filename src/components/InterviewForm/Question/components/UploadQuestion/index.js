import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Upload, Icon } from 'antd';
import UploadQuestionWrapper from './style';

const UploadQuestion = ({ source, form }) => (
    <UploadQuestionWrapper>
      <Form.Item>
        {form.getFieldDecorator(source, {
          rules: [
            {
              required: true,
              message: 'Plese upload file',
            },
          ],
        })(
          <Upload
            name="logo"
            action="/upload.do"
            listType="picture"
            className="customUpload"
          >
            <Button>
              <Icon type="upload" /> Click to upload
            </Button>
          </Upload>,
        )}
      </Form.Item>
    </UploadQuestionWrapper>
  );

UploadQuestion.propTypes = {
  source: PropTypes.string.isRequired,
  form: PropTypes.object.isRequired,
};

export default UploadQuestion;
