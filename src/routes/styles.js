import styled from 'styled-components';

export default styled.div`
  .ant-form-item {
    margin-bottom: 10px;
    text-align: left;
  }
`;
