import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import PrivateLayout from '../../layout/PrivateLayout';

import { PageContent } from '../../components/common';
import { EmployeeIcon } from '../../assets/icons';
import ModalLayout from '../../layout/ModalLayout';
import { Employee } from '../../pages';

export const PRIVATE_ROUTES = [
  {
    key: 'employees',
    exact: true,
    text: 'Employees',
    icon: EmployeeIcon,
    path: '/',
    routes: [
      {
        path: '/',
        title: 'employee.title',
        component: Employee.List
      }
    ],
    modals: [
      {
        path: '/create',
        component: Employee.Create,
        title: 'employee.modals.create'
      },
      {
        path: '/edit',
        component: Employee.Edit,
        title: 'employee.modals.edit'
      }
    ]
  }
];

export const MODAL_LISTS = [...PRIVATE_ROUTES].reverse(({ path, modals }) => ({
  path,
  modals
}));

const PrivateRoutes = () => (
  <PrivateLayout>
    <ModalLayout />
    <Switch>
      {PRIVATE_ROUTES.flatMap(
        route =>
          route.routes &&
          route.routes.map(subRoute => ({
            ...subRoute,
            path: route.path + subRoute.path,
            exact: subRoute.path === '/'
          }))
      ).map(route => (
        <Route
          {...route}
          component={e => (
            <>
              <PageContent title={route.title} {...e} path={route.path} />
              <route.component {...e} />
            </>
          )}
          key={route.path}
        />
      ))}
      <Redirect to="/" />
    </Switch>
  </PrivateLayout>
);

export default PrivateRoutes;
