import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import { connect } from 'react-redux';
import {
  closeModalAction,
  saveDraftAction
} from '../../../redux/modal/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const { TextArea } = Input;

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  closeModal,
  item,
  editPreviewInterview,
  id,
  draft,
  saveDraft
}) => {
  const { match, previewInterview } = item;
  const handleSubmit = e => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        editPreviewInterview(match.params.id, values);
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const value = getFieldsValue();
      saveDraft({ data: value, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item label="Title" colon={false}>
        {getFieldDecorator(
          'title',
          {
            initialValue: previewInterview.title || ''
          },
          {
            rules: [{ required: true, message: 'Please input your username!' }]
          }
        )(<Input />)}
      </Form.Item>
      <Form.Item label="Introduction" colon={false}>
        {getFieldDecorator(
          'introduce',
          {
            initialValue: previewInterview.introduce || ''
          },
          {
            rules: [{ required: true, message: 'Please input your username!' }]
          }
        )(<TextArea autosize={{ minRows: 1 }} />)}
      </Form.Item>
      <Form.Item>
        <Button type="primary" htmlType="submit" className="login-form-button">
          Confirm
        </Button>
        <Button onClick={() => closeModal(id)}>Cancel</Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.object.isRequired,
  editPreviewInterview: PropTypes.func.isRequired,
  closeModal: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  id: PropTypes.number
};

export default connect(
  (state, props) => ({
    item: itemSelector(state, props),
    draft: draftSelector(state, props)
  }),
  dispatch => ({
    closeModal: id => dispatch(closeModalAction(id)),
    saveDraft: id => dispatch(saveDraftAction(id))
  })
)(Form.create()(Edit));
