import React, { useEffect } from 'react';
import {
  Form,
  Button,
  Input,
  DatePicker,
  TimePicker,
  Col,
  Row,
  Select,
} from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { editAppointmentAction } from '../../../redux/appointments/actions';
import { draftSelector, itemSelector } from '../../../redux/modal/selectors';
import StyledForm from '../Create/style';
import { mergedDateAndTime } from '../../../utils/timeUtils';

import { getAllContactAction } from '../../../redux/contact/actions';

const { Option } = Select;

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  editAppointment,
  closeModal,
  id,
  draft,
  saveDraft,
  contacts,
  recordData,
  getAllContact,
}) => {
  const handSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const startTime = mergedDateAndTime(
          values.startDate,
          values.startTime
        ).getTime();
        const endTime = mergedDateAndTime(
          values.endDate,
          values.endTime
        ).getTime();
        editAppointment({
          id: recordData.id,
          appointment: {
            startTime,
            endTime,
            location: values.location,
            attendees: values.attendees,
            title: values.title,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    getAllContact();
  }, [getAllContact]);

  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  const handleChangeStartDate = e => {
    setFieldsValue({ endDate: e });
  };
  const handleChangeStartTime = e => {
    const newEndTime = moment(new Date(e).getTime() + 60 * 60 * 1000);
    setFieldsValue({ endTime: newEndTime });
  };
  return (
    <StyledForm>
      <Form onSubmit={handSubmit} className="login-form">
        <Form.Item label="Title" colon={false}>
          {getFieldDecorator(
            'title',
            {
              initialValue: recordData.title || null,
            },
            {
              rules: [{ required: true, message: i18n.t('messages.title') }],
            }
          )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
        </Form.Item>
        <Form.Item label="Location" colon={false}>
          {getFieldDecorator(
            'location',
            {
              initialValue: recordData.location || null,
            },
            {
              rules: [{ required: true, message: i18n.t('messages.address') }],
            }
          )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
        </Form.Item>
        <Row>
          <Col span={8}>
            <Form.Item label="Start Date" colon={false}>
              {getFieldDecorator(
                'startDate',
                {
                  initialValue: moment(recordData.startTime),
                },
                {
                  rules: [
                    {
                      required: true,
                      message: i18n.t('messages.date'),
                    },
                  ],
                }
              )(<DatePicker onChange={e => handleChangeStartDate(e)} />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="Start Time" colon={false}>
              {getFieldDecorator(
                'startTime',
                {
                  initialValue: moment(recordData.startTime),
                },
                {
                  rules: [
                    {
                      required: true,
                      message: i18n.t('messages.time'),
                    },
                  ],
                }
              )(<TimePicker onChange={e => handleChangeStartTime(e)} />)}
            </Form.Item>
          </Col>
        </Row>
        <Row>
          <Col span={8}>
            <Form.Item label="End Date" colon={false}>
              {getFieldDecorator(
                'endDate',
                {
                  initialValue: moment(recordData.endTime),
                },
                {
                  rules: [
                    {
                      required: true,
                      message: i18n.t('messages.date'),
                    },
                  ],
                }
              )(<DatePicker />)}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item label="EndTime" colon={false}>
              {getFieldDecorator(
                'endTime',
                {
                  initialValue: moment(recordData.endTime),
                },
                {
                  rules: [
                    {
                      required: true,
                      message: i18n.t('messages.time'),
                    },
                  ],
                }
              )(<TimePicker />)}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item label="Gmail">
          {getFieldDecorator(
            'attendees',
            {
              initialValue: recordData.attendees || null,
            },
            {
              rules: [
                {
                  required: true,
                  message: 'Please select your favourite colors!',
                  type: 'array',
                },
              ],
            }
          )(
            <Select mode="multiple">
              {contacts.map(contact => (
                <Option key={contact.id} value={contact.data.email}>
                  {contact.data.email}
                </Option>
              ))}
            </Select>
          )}
        </Form.Item>

        <Form.Item>
          <Button onClick={() => closeModal(id)}>
            {i18n.t('button.cancel')}
          </Button>
          <Button type="primary" htmlType="submit">
            {i18n.t('button.edit')}
          </Button>
        </Form.Item>
      </Form>
    </StyledForm>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editAppointment: PropTypes.func.isRequired,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  contacts: PropTypes.array,
  recordData: PropTypes.object,
  getAllContact: PropTypes.func.isRequired,
};

Edit.defaultProps = {
  contacts: [],
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
  recordData: itemSelector(state, props),
  contacts: state.contact.contacts,
});

const mapDispatchToProps = dispatch => ({
  getAllContact: () => dispatch(getAllContactAction()),
  closeModal: id => dispatch(closeModalAction(id)),
  editAppointment: params => dispatch(editAppointmentAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Edit));
