import React, { useEffect } from 'react';
import { Form, Button, Input } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { editPositionAction } from '../../../redux/positions/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editPosition,
  id,
  draft,
  closeModal,
  saveDraft,
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        editPosition({ id: recordData.id, position: values });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator(
          'name',
          {
            initialValue: recordData.name || null,
          },
          {
            rules: [{ required: true, message: 'Please enter position' }],
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editPosition: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  recordData: PropTypes.object,
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  editPosition: params => dispatch(editPositionAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Edit));
