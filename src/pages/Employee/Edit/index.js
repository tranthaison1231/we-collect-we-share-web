import React, { useEffect } from 'react';
import { Form, Button, Input, Upload, Icon, DatePicker, Select } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { editEmployeeAction } from '../../../redux/employees/actions';
import { uploadImageAction } from '../../../redux/firebaseImg/actions';
import { getAllPositionsAction } from '../../../redux/positions/actions';
import { itemSelector, draftSelector } from '../../../redux/modal/selectors';

const Edit = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  recordData,
  editEmployee,
  id,
  draft,
  closeModal,
  saveDraft,
  uploadImage,
  positions,
  getAllPositions,
}) => {
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const birthday = new Date(values.birthday).getTime();
        const dayInCompany = new Date(values.dayInCompany).getTime();
        editEmployee({
          id: recordData.id,
          employee: {
            name: values.name,
            job: values.job,
            email: values.email,
            birthday,
            dayInCompany,
            imgName: values.profilePic.file
              ? values.profilePic.file.uid
              : values.profilePic.uid,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    getAllPositions();
  }, [getAllPositions]);
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  const handleUpload = ({ onError, onSuccess, file }) => {
    try {
      uploadImage(file, onSuccess);
    } catch (error) {
      onError(error);
    }
  };
  return (
    <Form onSubmit={onSubmit} className="login-form">
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator(
          'name',
          {
            initialValue: recordData.name || null,
          },
          {
            rules: [{ required: true, message: 'Please enter role' }],
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Job" colon={false}>
        {getFieldDecorator(
          'job',
          {
            initialValue: recordData.job,
          },
          {
            rules: [{ required: true, message: 'Please enter role' }],
          }
        )(
          <Select>
            {positions.map(position => (
              <Select.Option key={position.id} value={position.name}>
                {position.name}
              </Select.Option>
            ))}
          </Select>
        )}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator(
          'email',
          {
            initialValue: recordData.email || null,
          },
          {
            rules: [{ required: true, message: 'Please enter role' }],
          }
        )(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Birthday" colon={false}>
        {getFieldDecorator(
          'birthday',
          {
            initialValue: moment(recordData.birthday),
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date'),
              },
            ],
          }
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Start Work Date" colon={false}>
        {getFieldDecorator(
          'dayInCompany',
          {
            initialValue: moment(recordData.dayInCompany),
          },
          {
            rules: [
              {
                required: true,
                message: i18n.t('messages.date'),
              },
            ],
          }
        )(<DatePicker />)}
      </Form.Item>
      <Form.Item label="Avatar" colon={false}>
        {getFieldDecorator('profilePic', {
          initialValue: {
            status: 'done',
            uid: recordData.imgName,
          },
        })(
          <Upload name="logo" customRequest={handleUpload} listType="picture">
            <Button>
              <Icon type="upload" />
            </Button>
          </Upload>
        )}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.edit')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Edit.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  editEmployee: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  recordData: PropTypes.object,
  uploadImage: PropTypes.func,
  getAllPositions: PropTypes.func,
  positions: PropTypes.array,
};

const mapStateToProps = (state, props) => ({
  recordData: itemSelector(state, props),
  draft: draftSelector(state, props),
  positions: state.positions.positions,
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  editEmployee: params => dispatch(editEmployeeAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
  uploadImage: (file, onSuccess) =>
    dispatch(uploadImageAction(file, onSuccess)),
  getAllPositions: () => dispatch(getAllPositionsAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Edit));
