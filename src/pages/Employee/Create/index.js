import React, { useEffect } from 'react';
import { Form, Button, Input, Select } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { draftSelector } from '../../../redux/modal/selectors';
import { getAllPositionsAction } from '../../../redux/positions/actions';
import { createEmployeeAction } from '../../../redux/employees/actions';
import { CustomUpload } from '../../../containers/common';
import CustomDatePicker from '../../../components/common/CustomDatePicker';

const { Option } = Select;

const Create = ({
  form,
  createEmployee,
  positions,
  closeModal,
  id,
  saveDraft,
  getAllPosition,
  draft,
}) => {
  const {
    validateFields,
    getFieldDecorator,
    setFieldsValue,
    getFieldsValue,
  } = form;
  const onSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        createEmployee({
          employee: {
            name: values.name,
            job: values.job,
            email: values.email,
            birthday: new Date(values.birthday).getTime(),
            dayInCompany: new Date(values.dayInCompany).getTime(),
            imgName: values.imgURL.file.uid,
          },
        });
        closeModal(id);
      }
    });
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);

  useEffect(() => {
    getAllPosition();
  }, [getAllPosition]);

  return (
    <Form onSubmit={onSubmit}>
      <Form.Item label="Employee Id" colon={false}>
        {getFieldDecorator('id', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input />)}
      </Form.Item>
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator('name', {
          rules: [{ required: true, message: 'Please enter email' }],
        })(<Input />)}
      </Form.Item>
      <CustomDatePicker
        form={form}
        label="Start Work Date"
        colon={false}
        src="dayInCompany"
        required
        message="Please enter date"
      />
      <CustomDatePicker
        form={form}
        label="BirthDay"
        colon={false}
        src="birthday"
        required
        message="Please enter date"
      />
      <Form.Item label="Position" colon={false}>
        {getFieldDecorator('job', {
          rules: [
            {
              required: true,
              message: 'Please select your job!',
            },
          ],
        })(
          <Select>
            {positions.map(position => (
              <Option key={position.id} value={position.name}>
                {position.name}
              </Option>
            ))}
          </Select>
        )}
      </Form.Item>
      <CustomUpload
        form={form}
        label="Avatar"
        src="imgURL"
        colon={false}
        message="Please up your image"
        required
      />
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  createEmployee: PropTypes.func,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
  id: PropTypes.number,
  positions: PropTypes.array,
  getAllPosition: PropTypes.func.isRequired,
};

Create.defaultProps = {
  positions: [],
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
  positions: state.positions.positions,
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  createEmployee: params => dispatch(createEmployeeAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
  getAllPosition: () => dispatch(getAllPositionsAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Create));
