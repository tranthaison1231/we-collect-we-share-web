import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Tag, Tooltip, Icon } from 'antd';
import i18n from 'i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  closeModalAction,
  saveDraftAction,
} from '../../../redux/modal/actions';
import { addContactAction } from '../../../redux/contact/actions';
import { draftSelector } from '../../../redux/modal/selectors';

const Create = ({
  form: { validateFields, getFieldDecorator, setFieldsValue, getFieldsValue },
  addContact,
  closeModal,
  id,
  draft,
  saveDraft,
}) => {
  const [tags, setTags] = useState([]);
  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState('');

  const handleClose = removedTag => {
    const filteredTags = tags.filter(tag => tag !== removedTag);
    setTags(filteredTags);
  };
  useEffect(() => {
    if (draft) {
      setFieldsValue(draft);
    }
    return () => {
      const values = getFieldsValue();
      saveDraft({ data: values, id });
    };
  }, [draft, getFieldsValue, id, saveDraft, setFieldsValue]);
  const showInput = () => {
    setInputVisible(true, () => this.input.focus());
  };

  const handleInputChange = e => {
    setInputValue(e.target.value);
  };

  const handleInputConfirm = () => {
    let tagsFinal;
    if (inputValue && tags.indexOf(inputValue) === -1) {
      tagsFinal = [...tags, inputValue];
    }
    setTags(tagsFinal);
    setInputValue('');
    setInputVisible(false);
  };

  const saveInputRef = input => input;
  const handleSubmit = e => {
    e.preventDefault();
    validateFields((error, values) => {
      if (!error) {
        const data = { ...values, ...{ position: tags } };
        addContact({ contact: data });
        closeModal(id);
      }
    });
  };
  return (
    <Form onSubmit={handleSubmit} className="login-form">
      <Form.Item label="Name" colon={false}>
        {getFieldDecorator('name', {
          rules: [{ required: true, message: i18n.t('message.name') }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Email" colon={false}>
        {getFieldDecorator('email', {
          rules: [{ required: true, message: i18n.t('messages.email') }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Phone Number" colon={false}>
        {getFieldDecorator('phone', {
          rules: [{ required: true, message: i18n.t('messages.phone') }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item label="Company" colon={false}>
        {getFieldDecorator('company', {
          rules: [{ required: true, message: i18n.t('messages.company') }],
        })(<Input style={{ color: 'rgba(0,0,0,.25)' }} />)}
      </Form.Item>
      <Form.Item>
        {tags.map((tag, index) => {
          const isLongTag = tag.length > 20;
          const tagElem = (
            <Tag
              key={tag}
              closable={index !== 0}
              onClose={() => handleClose(tag)}
            >
              {isLongTag ? `${tag.slice(0, 20)}...` : tag}
            </Tag>
          );
          return isLongTag ? (
            <Tooltip title={tag} key={tag}>
              {tagElem}
            </Tooltip>
          ) : (
            tagElem
          );
        })}
        {inputVisible && (
          <Input
            ref={saveInputRef}
            type="text"
            size="small"
            style={{ width: 78 }}
            value={inputValue}
            onChange={handleInputChange}
            onBlur={handleInputConfirm}
            onPressEnter={handleInputConfirm}
          />
        )}
        {!inputVisible && (
          <Tag
            onClick={showInput}
            style={{ background: '#fff', borderStyle: 'dashed' }}
          >
            <Icon type="plus" /> New Position
          </Tag>
        )}
      </Form.Item>
      <Form.Item>
        <Button onClick={() => closeModal(id)}>
          {i18n.t('button.cancel')}
        </Button>
        <Button type="primary" htmlType="submit">
          {i18n.t('button.create')}
        </Button>
      </Form.Item>
    </Form>
  );
};

Create.propTypes = {
  form: PropTypes.any,
  closeModal: PropTypes.func,
  addContact: PropTypes.func,
  id: PropTypes.number,
  draft: PropTypes.object,
  saveDraft: PropTypes.func,
};

const mapStateToProps = (state, props) => ({
  draft: draftSelector(state, props),
});

const mapDispatchToProps = dispatch => ({
  closeModal: id => dispatch(closeModalAction(id)),
  addContact: params => dispatch(addContactAction(params)),
  saveDraft: params => dispatch(saveDraftAction(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form.create()(Create));
